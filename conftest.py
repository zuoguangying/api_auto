﻿from pathlib import Path


# #获取当前路径
# test_path = str(Path(__file__).parent)
# print(test_path.split("testcases")[0])

from pathlib import Path

import pytest

from commons.yaml_util import clear_extract_yaml

@pytest.fixture(scope="session",autouse=True)
def auto_clear_extract_yaml():
    clear_extract_yaml()