#读取用例
import logger
import yaml


def read_testcase_yaml(yaml_path):
    with open(yaml_path,encoding="utf-8",mode="r") as f:
        case_list=yaml.safe_load(f)
        #流程用例
        if len(case_list)>=2:
            return case_list
        else:
            if "parametrize" in dict(*case_list).keys():
                new_caseinfo=ddts(*case_list,yaml_path.name)
                return new_caseinfo
            else:
                return case_list
#处理parametrize数据驱动：{parametrize}改成[{}，{}，{}]
def ddts(case:dict,yaml_name):
    param_list=case["parametrize"]
    #判断parametrize不为空
    if param_list:
        name_length=len(param_list[0])
        for p in param_list:
            if len(p)!=name_length:
                logger.error(yaml_name+"parametrize数据驱动中数据或名称长度不一致\n")
                break
            else:
                #如果长度没问题
                new_caseinfo_list=[]
                #把字典转换成字符串，因为只有字符型才能被替换
                case_str=yaml.safe_dump(case)
                #循环
                for x in range(1,len(param_list)):
                    #执行前先保存一份case
                    raw_caseinfo=case_str
                    for y in range(0,name_length):
                        #处理数据类型，如果是数字类型的字符串则加一对单引号
                        if isinstance(param_list[x][y],str) and param_list[x][y].isdigit():
                            param_list[x][y]="'"+param_list[x][y]+"'"
                        #替换
                        raw_caseinfo=raw_caseinfo.replace("$ddt{"+param_list[0][y]+"}",str(param_list[x][y]))
                    #加入新的list中
                    raw_case_dict=yaml.safe_load(raw_caseinfo)
                    raw_case_dict.pop("parametrize")
                    new_caseinfo_list.append(raw_case_dict)
                return new_caseinfo_list
    else:
        return [case]
