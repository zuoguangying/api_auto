import logging

import requests

from configs import setting

logger=logging.getLogger(__name__)
class RequestUtil:
    #类变量和实例变量
    sess=requests.session()
    #统一封装请求接口，self是当前类的对象，和Java中的this一样
    def send_all_request(self, base_url, **kwargs):
        #处理参数
        for args_key,args_value in kwargs.items():
            #处理基础路径如果是["url"]
            url=base_url+kwargs["url"]
            #处理公共参数
            if args_key=="params":
                kwargs["params"].updata(setting.global_args)
            #处理files
            try:
                if args_key=="files":
                    for key,value in args_value.items():
                        args_value[key]=open(value,"rb")
            except Exception:
                logger.error("文件路径找不到！\n")
                raise
            #打印请求参数日志
            logger.info("请求"+args_key+"参数:%s"%args_value)
        #发送请求
        res=RequestUtil.sess.request(**kwargs)
        print(type(res))
        print(res)
        if "json" in res.headers.get("Content-Type"):
            logger.info("响应正文:%s"%res.json())
        elif "form-data" in res.headers.get("Content-Type"):
            logger.info("响应正文:%s" % res.text)
        else:
            #打印响应参数
            logger.info("响应正文:太长暂不显示")
        return res


