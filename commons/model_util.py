#封装的规范YAML以及校验YAML的类
#dataclass是类在实例化过程中(调用__init__方法时给属性赋值)自动传参和自动校验的工具类
from dataclasses import dataclass

import logger


@dataclass
class CaseInfo:
    #必填
    feature:str
    story:str
    title:str
    request:str
    validate:str
    #选填
    extract:dict=None
    parametrize:list=None
#校验yaml格式
def verify_yaml(case: dict,yaml_name):
    try:
        new_case=CaseInfo(**case)
        return new_case
    except Exception:
        logger.error(yaml_name+":YAML格式不符合框架规则\n")
        raise Exception("YAML格式不符合框架规则!")