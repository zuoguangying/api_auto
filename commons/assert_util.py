import copy

import pymysql
import yaml

from configs import setting


class AssertUtil:
    #创建数据库链接，pymysql和mysqlclient
    def create_connction(self):
        self.conn=pymysql.connect(
            user=setting.db_user,
            password=setting.db_password,
            host=setting.db_host,
            database=setting.db_batabase,
            port=setting.db_port
        )
        return self.conn
    #执行sql语句
    def execute_sql(self, sql):
        #创建链接
        conn=self.create_connction()
        #创建游标
        cs=conn.cursor()
        #执行sql
        cs.execute(sql)
        #取值
        value=cs.fetchone()
        #关闭
        cs.close()
        conn.close()
        return value
    #处理断言
    def assert_all(self, resp,assert_key,assert_value):
       # 1.深拷贝一个response(深拷贝，浅拷贝)
        resp=copy.deepcopy(resp)
       #把json（）改成一个属性
        try:
            resp.json=resp.json()
        except Exception:
            resp.json={"msg":"response is not json"}
        #获取实际结果(sj_data)和预期结果(yq_data)
        sj_result,yq_result = assert_value[0],assert_value[1]
       #通过属性反射获取到属性的数据
        sj_data=getattr(resp,sj_result)
       #实际结果类型从字典转换成字符串
        if isinstance(sj_data,dict):
            sj_data=yaml.dump(sj_data,allow_unicode=True)
        #断言
        match assert_key:
            case "equals":
                assert sj_data==yq_result
            case "contains":
                assert yq_result in sj_data
            case "db_equals":
                value = self.execute_sql(yq_result)
            case "db_contains":
                value = self.execute_sql(yq_result)
                if value:
                    assert value[0] in sj_data

