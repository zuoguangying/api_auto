#前后置脚本
import base64
import hashlib
import json
import time

import rsa
import yaml

from configs import setting
from Crypto.Cipher import PKCS1_v1_5 as Cipher_pkcs1_v1_5
import base64
from datetime import datetime
from Crypto.PublicKey import RSA

class DebugTalk:
    #读取extract.yaml文件
    def read_extract(self, key):
        with open(setting.extract_file_name,encoding="utf-8",mode="r") as f:
            yaml_data=yaml.safe_load(f)
            return yaml_data[key]
    #随机数
    def get_times(self):
        return int(time.time())
    #MD5加密
    def md5_encode(self, args):
        #把变量转换成utf-8的编码格式
        utf8_str=str(args).encode("utf-8")
        #md5加密
        md5_value=hashlib.md5(utf8_str).hexdigest()
        return md5_value
    #Base64加密
    def base64_encode(self, args):
        # 把变量转换成utf-8的编码格式
        utf8_str=str(args).encode("utf-8")
        #bas64加密
        base64_value=base64.b64decode(utf8_str).decode(encoding="utf-8")
        return base64_value
    # #生成rsa的公钥和私钥
    # def create_key(self):
    #     (public_key,private_key)=rsa.newkeys(nbits=1024)
    #     #保存公钥
    #     with open("./public.pem","w+") as f:
    #         f.write(public_key.save_pkcs1().decode())
    #     #保存私钥
    #     with open("./private.pem","w+") as f:
    #         f.write(private_key.save_pkcs1().decode())
    #rsa加密
    def rsa_encode(self, args):
        #获取公钥
        with open("./public.pem","r") as f:
            pubkey=rsa.PublicKey.load_pkcs1(f.read().encode())
        #把变量转换成utf-8
        utf8_str=str(args).encode("utf-8")
        #把字符串进行rsa加密
        byte_value=rsa.encrypt(utf8_str,pubkey)
        #把字节类型转换成字符串的值
        rsa_str=base64.b64decode(byte_value).decode("utf-8")
        return  rsa_str

    def rsa(self, args):
        key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCs4q7+zmykmotvqqfz1Im39v9jIv2SCRcrbwvH889vc8aAR9yZ1ZuE8LySYpyFjoV5+hrtcIf4NNLbyhoRNgA9wAkLtb0XUPQEyRHZh+v4ZHg2IWItswPu61qrQg+CRtCY+KZUM9eyZsuf/UJuB2A8s4wODoEpVTlDIIaqVDZlMQIDAQAB'
        pub_key = '-----BEGIN PUBLIC KEY-----\n' + key + '\n' + '-----END PUBLIC KEY-----'
        pubkey = rsa.PublicKey.load_pkcs1_openssl_pem(pub_key.encode())
        # 把变量转换成utf-8
        utf8_str = str(args).encode("utf-8")
        # 把字符串进行rsa加密
        byte_value = rsa.encrypt(utf8_str, pubkey)
        # 把字节类型转换成字符串的值
        rsa_str = base64.b64encode(byte_value).decode("utf-8")
        return rsa_str

    def login_rsa(self,args):
        username = '18121225109'
        passwd = '123456'

        timenow = datetime.now().strftime('%Y%m%d%H%M%S')
        ori_data = [username, passwd, timenow]
        ori_data = {
            "phonenumber": username,
            "password": passwd,
            "timeSpan": timenow
        }
        ori_data = json.dumps(ori_data)
        key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCs4q7+zmykmotvqqfz1Im39v9jIv2SCRcrbwvH889vc8aAR9yZ1ZuE8LySYpyFjoV5+hrtcIf4NNLbyhoRNgA9wAkLtb0XUPQEyRHZh+v4ZHg2IWItswPu61qrQg+CRtCY+KZUM9eyZsuf/UJuB2A8s4wODoEpVTlDIIaqVDZlMQIDAQAB'
        pub_key = '-----BEGIN PUBLIC KEY-----\n' + key + '\n' + '-----END PUBLIC KEY-----'
        rsakey = RSA.importKey(pub_key)
        cipher = Cipher_pkcs1_v1_5.new(rsakey)
        login_data = base64.b64encode(cipher.encrypt(ori_data.encode('utf-8')))
        login_data = login_data.decode('utf-8')
        # url = 'https://99.in-road.com/oaa/api/Account/LoginSecure'
        login_data = {"data": login_data}
        return login_data
    #处理基础路径
    @classmethod
    def get_url(self,base_url):
        base_url="http://99.in-road.com"

        test=base_url
        return test


